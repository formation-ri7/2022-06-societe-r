<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220616131711 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE work_station ADD category_id INT NOT NULL');
        $this->addSql('ALTER TABLE work_station ADD CONSTRAINT FK_4AF2409312469DE2 FOREIGN KEY (category_id) REFERENCES category (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_4AF2409312469DE2 ON work_station (category_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE work_station DROP CONSTRAINT FK_4AF2409312469DE2');
        $this->addSql('DROP INDEX IDX_4AF2409312469DE2');
        $this->addSql('ALTER TABLE work_station DROP category_id');
    }
}
