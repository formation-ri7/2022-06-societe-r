<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220617073720 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE assignment_employee_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE assignment_employee (id INT NOT NULL, employee_id INT NOT NULL, work_station_id INT NOT NULL, started_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, ended_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, update_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_999D01548C03F15C ON assignment_employee (employee_id)');
        $this->addSql('CREATE INDEX IDX_999D01543ADE1FE6 ON assignment_employee (work_station_id)');
        $this->addSql('COMMENT ON COLUMN assignment_employee.started_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN assignment_employee.ended_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN assignment_employee.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN assignment_employee.update_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE assignment_employee ADD CONSTRAINT FK_999D01548C03F15C FOREIGN KEY (employee_id) REFERENCES person (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE assignment_employee ADD CONSTRAINT FK_999D01543ADE1FE6 FOREIGN KEY (work_station_id) REFERENCES work_station (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE assignment_employee_id_seq CASCADE');
        $this->addSql('DROP TABLE assignment_employee');
    }
}
