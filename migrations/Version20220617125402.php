<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220617125402 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE employee_skill (employee_id INT NOT NULL, skill_id INT NOT NULL, PRIMARY KEY(employee_id, skill_id))');
        $this->addSql('CREATE INDEX IDX_B630E90E8C03F15C ON employee_skill (employee_id)');
        $this->addSql('CREATE INDEX IDX_B630E90E5585C142 ON employee_skill (skill_id)');
        $this->addSql('ALTER TABLE employee_skill ADD CONSTRAINT FK_B630E90E8C03F15C FOREIGN KEY (employee_id) REFERENCES person (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE employee_skill ADD CONSTRAINT FK_B630E90E5585C142 FOREIGN KEY (skill_id) REFERENCES skill (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE person ADD main_skill_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE person ADD CONSTRAINT FK_34DCD1768CA7D037 FOREIGN KEY (main_skill_id) REFERENCES skill (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_34DCD1768CA7D037 ON person (main_skill_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP TABLE employee_skill');
        $this->addSql('ALTER TABLE person DROP CONSTRAINT FK_34DCD1768CA7D037');
        $this->addSql('DROP INDEX IDX_34DCD1768CA7D037');
        $this->addSql('ALTER TABLE person DROP main_skill_id');
    }
}
