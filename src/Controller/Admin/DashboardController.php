<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Employee;
use App\Entity\Person;
use App\Entity\Skill;
use App\Entity\Trainee;
use App\Entity\WorkStation;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public function __construct(private AdminUrlGenerator $routeBuilder)
    {
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $url = $this->routeBuilder->setController(PersonCrudController::class)->generateUrl();

        return $this->redirect($url);
        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('2022 06 Societe R')
        ;
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToCrud('Category', 'fas fa-home', Category::class);

        yield MenuItem::linkToCrud('Person', 'fas fa-home', Person::class);

        yield MenuItem::linkToCrud('Work Station', 'fas fa-home', WorkStation::class);

        yield MenuItem::linkToCrud('Skill', 'fas fa-home', Skill::class);

        yield MenuItem::linkToCrud('Employee', 'fas fa-home', Employee::class);

        yield MenuItem::linkToCrud('Trainee', 'fas fa-home', Trainee::class);

        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
