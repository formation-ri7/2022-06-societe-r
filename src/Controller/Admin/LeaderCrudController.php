<?php

namespace App\Controller\Admin;

use App\Entity\Leader;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class LeaderCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Leader::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
