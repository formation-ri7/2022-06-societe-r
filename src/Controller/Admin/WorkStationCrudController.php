<?php

namespace App\Controller\Admin;

use App\Entity\WorkStation;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class WorkStationCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return WorkStation::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
