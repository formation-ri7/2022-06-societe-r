<?php

namespace App\Controller\Api;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmployeeController extends AbstractController
{
    #[Route('/api/employee', name: 'app_api_employee')]
    public function index(): Response
    {
        return $this->render('api_employee/index.html.twig', [
            'controller_name' => 'EmployeeController',
        ]);
    }
}
