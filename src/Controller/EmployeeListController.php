<?php

namespace App\Controller;

use App\Repository\EmployeeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EmployeeListController extends AbstractController
{
    #[Route('/employee', name: 'app_employee_list')]
    public function show(EmployeeRepository $employeeRepository): Response
    {
        return $this->render('employee_list/index.html.twig', [
            'employees' => $employeeRepository->findAll(),
        ]);
    }
}
