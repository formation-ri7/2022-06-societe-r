<?php

namespace App\DataFixtures;

use App\Entity\Candidate;
use App\Entity\Skill;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CandidateFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 20; ++$i) {
            /** @var Skill $php */
            $php = $this->getReference('php');
            $candidate = new Candidate();
            $candidate->setName($faker->name());
            $candidate->setDescription($faker->paragraph());
            $candidate->setBirthDate(new DateTimeImmutable('1960-01-01 00:00:00'));
            $candidate->setCreatedAt(new DateTimeImmutable('2018-01-01 00:00:00'));
            $candidate->setUpdateAt(new DateTimeImmutable('2018-01-01 00:00:00'));
            $candidate->setAppointmentAt(new DateTimeImmutable('2018-01-01 00:00:00'));
            $candidate->addSkill($php);
        }

        $manager->flush();
    }

    /**
     * @return array<string>
     */
    public function getDependencies(): array
    {
        return [SkillFixtures::class];
    }
}