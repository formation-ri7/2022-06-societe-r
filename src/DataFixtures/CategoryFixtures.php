<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $webDevelopment = new Category();
        $webDevelopment->setNom('Web development');
        $webDevelopment->setDescription('lorem faker');
        $manager->persist($webDevelopment);

        $mobileDevelopment = new Category();
        $mobileDevelopment->setNom('Mobile development');
        $mobileDevelopment->setDescription('lorem faker');
        $manager->persist($mobileDevelopment);

        $qualityAssurance = new Category();
        $qualityAssurance->setNom('Quality assurance');
        $qualityAssurance->setDescription('lorem faker');
        $manager->persist($qualityAssurance);

        $marketing = new Category();
        $marketing->setNom('Marketing');
        $marketing->setDescription('lorem faker');
        $manager->persist($marketing);

        $applicationDevelopment = new Category();
        $applicationDevelopment->setNom('Application development');
        $applicationDevelopment->setDescription('lorem faker');
        $manager->persist($applicationDevelopment);

        $commercialDepartment = new Category();
        $commercialDepartment->setNom('Commercial department');
        $commercialDepartment->setDescription('lorem faker');
        $manager->persist($commercialDepartment);

        $manager->flush();
    }
}
