<?php

namespace App\DataFixtures;

use App\Entity\Employee;
use App\Entity\Team;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class EmployeeFixtures extends Fixture implements DependentFixtureInterface
{
    public const EMPLOYEE_REFERENCE = 'employee';

    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 51; ++$i) {
            /**
             * @var Team $team
             */
            $team = $this->getReference(Team::class.rand(1, 5));
            $employee = new Employee();
            $employee->setName($faker->name());
            $employee->setDescription($faker->paragraph());
            $employee->setBirthDate(new DateTimeImmutable('1960-01-01 00:00:00'));
            $employee->setCreatedAt(new DateTimeImmutable('2018-01-01 00:00:00'));
            $employee->setUpdateAt(new DateTimeImmutable('2018-01-01 00:00:00'));
            $employee->setStartedAt(new DateTimeImmutable('2018-01-01 00:00:00'));
            $employee->setSalary(rand(2000, 3000));
            $employee->setTeam($team);
            $manager->persist($employee);
        }

        $manager->flush();

        $this->addReference(self::EMPLOYEE_REFERENCE, $employee);
    }

    /**
     * @return array<string>
     */
    public function getDependencies(): array
    {
        return [TeamFixtures::class];
    }
}
