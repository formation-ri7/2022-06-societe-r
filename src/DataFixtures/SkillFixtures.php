<?php

namespace App\DataFixtures;

use App\Entity\Skill;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class SkillFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $php = new Skill();
        $php->setName('PHP');
        $php->setDescription('lorem');
        $manager->persist($php);

        $javascript = new Skill();
        $javascript->setName('Javascript');
        $javascript->setDescription('lorem');
        $manager->persist($javascript);

        $python = new Skill();
        $python->setName('Python');
        $python->setDescription('lorem');
        $manager->persist($python);

        $sql = new Skill();
        $sql->setName('SQL');
        $sql->setDescription('lorem');
        $manager->persist($sql);

        $html_css = new Skill();
        $html_css->setName('HTML/CSS');
        $html_css->setDescription('lorem');
        $manager->persist($html_css);

        $docker = new Skill();
        $docker->setName('Docker');
        $docker->setDescription('lorem');
        $manager->persist($docker);

        $kubernetes = new Skill();
        $kubernetes->setName('Kubernetes');
        $kubernetes->setDescription('lorem');
        $manager->persist($kubernetes);

        $project_analyse = new Skill();
        $project_analyse->setName('Project analyse');
        $project_analyse->setDescription('lorem');
        $manager->persist($project_analyse);

        $symfony = new Skill();
        $symfony->setName('Symfony');
        $symfony->setDescription('lorem');
        $manager->persist($symfony);

        $vuejs = new Skill();
        $vuejs->setName('Vue.js');
        $vuejs->setDescription('lorem');
        $manager->persist($vuejs);

        $reactjs = new Skill();
        $reactjs->setName('React.js');
        $reactjs->setDescription('lorem');
        $manager->persist($reactjs);

        $angularjs = new Skill();
        $angularjs->setName('Angular.js');
        $angularjs->setDescription('lorem');
        $manager->persist($angularjs);

        $sveltejs = new Skill();
        $sveltejs->setName('Svelte.js');
        $sveltejs->setDescription('lorem');
        $manager->persist($sveltejs);

        $manager->flush();
        $this->addReference('php', $php);
    }
}
