<?php

namespace App\DataFixtures;

use App\Entity\Team;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TeamFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 6; ++$i) {
            $team = new Team();
            $team->setName($faker->name());
            $team->setColor($faker->colorName());
            $team->setCreatedAt(new DateTimeImmutable());
            $team->setUpdatedAt(new DateTimeImmutable());
            $manager->persist($team);
            $manager->flush();
            $this->addReference(Team::class.$i, $team);
        }
    }
}
