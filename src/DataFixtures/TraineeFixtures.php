<?php

namespace App\DataFixtures;

use App\Entity\Trainee;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class TraineeFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 51; ++$i) {
            $trainee = new Trainee();
            $trainee->setName($faker->name());
            $trainee->setDescription($faker->paragraph());
            $trainee->setBirthDate(new DateTimeImmutable('1960-01-01 00:00:00'));
            $trainee->setCreatedAt(new DateTimeImmutable('2018-01-01 00:00:00'));
            $trainee->setUpdateAt(new DateTimeImmutable('2018-01-01 00:00:00'));
            $trainee->setSchoolName($faker->name());
        }

        $manager->flush();
    }
}
