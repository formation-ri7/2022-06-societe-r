<?php

namespace App\Entity;

use App\Repository\EmployeeRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

#[ORM\Entity(repositoryClass: EmployeeRepository::class)]
class Employee extends Person
{
    #[NotBlank]
    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeImmutable $startedAt;

    #[NotBlank]
    #[Range(min: 1000, max: 3000)]
    #[ORM\Column(type: 'float')]
    private float $salary;

    #[ORM\OneToOne(mappedBy: 'employee', targetEntity: User::class, cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(nullable: true)]
    private ?User $userEmployee = null;

    #[ORM\ManyToMany(targetEntity: self::class, inversedBy: 'workWithMe')]
    private Collection $workWith;

    #[ORM\ManyToMany(targetEntity: self::class, mappedBy: 'workWith')]
    private Collection $workWithMe;
    #[ORM\ManyToOne(targetEntity: Team::class, inversedBy: 'employees')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Team $team = null;

    #[ORM\ManyToOne(targetEntity: Skill::class)]
    private ?Skill $mainSkill = null;

    #[ORM\ManyToMany(targetEntity: Skill::class)]
    private collection $secondarySkills;

    public function __construct()
    {
        parent::__construct();

        $this->workWith = new ArrayCollection();
        $this->workWithMe = new ArrayCollection();
        $this->secondarySkills = new ArrayCollection();
    }

//    #[ORM\OneToMany(mappedBy: 'employee', targetEntity: AssignmentEmployee::class)]
//    private ArrayCollection $assignmentEmployees;

    public function getStartedAt(): ?DateTimeImmutable
    {
        return $this->startedAt;
    }

    public function setStartedAt(DateTimeImmutable $startedAt): self
    {
        $this->startedAt = $startedAt;

        return $this;
    }

    public function getSalary(): ?float
    {
        return $this->salary;
    }

    public function setSalary(float $salary): self
    {
        $this->salary = $salary;

        return $this;
    }

    public function getUserEmployee(): ?User
    {
        return $this->userEmployee;
    }

    public function setUserEmployee(?User $userEmployee): self
    {
        // unset the owning side of the relation if necessary
        if (null === $userEmployee && null !== $this->userEmployee) {
            $this->userEmployee->setEmployee(null);
        }

        // set the owning side of the relation if necessary
        if (null !== $userEmployee && $userEmployee->getEmployee() !== $this) {
            $userEmployee->setEmployee($this);
        }

        $this->userEmployee = $userEmployee;

        return $this;
    }

//    /**
//     * @return Collection<int, AssignmentEmployee>
//     */
//    public function getAssignmentEmployees(): Collection
//    {
//        return $this->assignmentEmployees;
//    }
//
//    public function addAssignmentEmployee(AssignmentEmployee $assignmentEmployees): self
//    {
//        if (!$this->assignmentEmployees->contains($assignmentEmployees)) {
//            $this->assignmentEmployees[] = $assignmentEmployees;
//            $assignmentEmployees->setEmployee($this);
//        }
//
//        return $this;
//    }
//
//    public function removeAssignmentEmployee(AssignmentEmployee $assignmentEmployees): self
//    {
//        if ($this->assignmentEmployees->removeElement($assignmentEmployees)) {
//            // set the owning side to null (unless already changed)
//            if ($assignmentEmployees->getEmployee() === $this) {
//                $assignmentEmployees->setEmployee(null);
//            }
//        }
//
//        return $this;
//    }

    /**
     * @return Collection<int, self>
     */
    public function getWorkWith(): Collection
    {
        return $this->workWith;
    }

    public function addWorkWith(self $workWith): self
    {
        if (!$this->workWith->contains($workWith)) {
            $this->workWith[] = $workWith;
        }

        return $this;
    }

    public function removeWorkWith(self $workWith): self
    {
        $this->workWith->removeElement($workWith);

        return $this;
    }

    /**
     * @return Collection<int, self>
     */
    public function getWorkWithMe(): Collection
    {
        return $this->workWithMe;
    }

    public function addWorkWithMe(self $workWithMe): self
    {
        if (!$this->workWithMe->contains($workWithMe)) {
            $this->workWithMe[] = $workWithMe;
            $workWithMe->addWorkWith($this);
        }

        return $this;
    }

    public function removeWorkWithMe(self $workWithMe): self
    {
        if ($this->workWithMe->removeElement($workWithMe)) {
            $workWithMe->removeWorkWith($this);
        }

        return $this;
    }

    public function getMainSkill(): ?Skill
    {
        return $this->mainSkill;
    }

    public function setMainSkill(?Skill $mainSkill): self
    {
        $this->mainSkill = $mainSkill;

        return $this;
    }

    /**
     * @return Collection<int, Skill>
     */
    public function getSecondarySkills(): Collection
    {
        return $this->secondarySkills;
    }

    public function addSecondarySkill(Skill $secondarySkill): self
    {
        if (!$this->secondarySkills->contains($secondarySkill)) {
            $this->secondarySkills[] = $secondarySkill;
            $secondarySkill->setEmployee($this);
        }

        return $this;
    }

    public function removeSecondarySkill(Skill $secondarySkill): self
    {
        if ($this->secondarySkills->removeElement($secondarySkill)) {
            // set the owning side to null (unless already changed)
            if ($secondarySkill->getEmployee() === $this) {
                $secondarySkill->setEmployee(null);
            }
        }
        
    }

    public function getTeam(): ?Team
    {
        return $this->team;
    }

    public function setTeam(?Team $team): self
    {
        $this->team = $team;

        return $this;
    }
}
