<?php

namespace App\Entity;

use App\Repository\LeaderRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: LeaderRepository::class)]
class Leader extends Employee
{
    #[ORM\Column(type: 'string', length: 100)]
    private ?string $title;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
