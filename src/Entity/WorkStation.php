<?php

namespace App\Entity;

use App\Repository\WorkStationRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WorkStationRepository::class)]
class WorkStation
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private int $id;

    #[ORM\Column(type: 'string', length: 50)]
    private ?string $label = null;

    #[ORM\Column(type: 'text')]
    private ?string $description = null;

    #[ORM\Column(type: 'boolean')]
    private ?bool $enabled = null;

    #[ORM\ManyToOne(targetEntity: Category::class, inversedBy: 'workStations')]
    #[ORM\JoinColumn(nullable: false)]
    private ?Category $category = null;

    #[ORM\OneToMany(mappedBy: 'workStation', targetEntity: AssignmentEmployee::class)]
    private ArrayCollection $assignmentEmployees;

    public function __construct()
    {
        $this->assignmentEmployees = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection<int, AssignmentEmployee>
     */
    public function getAssignmentEmployees(): Collection
    {
        return $this->assignmentEmployees;
    }

    public function addAssignmentEmployee(AssignmentEmployee $assignmentEmployees): self
    {
        if (!$this->assignmentEmployees->contains($assignmentEmployees)) {
            $this->assignmentEmployees[] = $assignmentEmployees;
            $assignmentEmployees->setWorkStation($this);
        }

        return $this;
    }

    public function removeAssignmentEmployee(AssignmentEmployee $assignmentEmployees): self
    {
        if ($this->assignmentEmployees->removeElement($assignmentEmployees)) {
            // set the owning side to null (unless already changed)
            if ($assignmentEmployees->getWorkStation() === $this) {
                $assignmentEmployees->setWorkStation(null);
            }
        }

        return $this;
    }
}
