<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
final class EmployeeControllerTest extends WebTestCase
{
    public function testIndex():void
    {
        $client = static::createClient();
        $client->request('GET', '/employee');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('p', 'Lorem Ipsum sum sum blablabla notre équipe est trop bien');
    }
}
